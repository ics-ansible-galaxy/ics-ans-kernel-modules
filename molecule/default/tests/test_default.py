import os
import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_kernel_rt(host):
    cmd = host.run("uname -a")
    assert "PREEMPT RT" in cmd.stdout


@pytest.mark.parametrize("name", ["dkms", "ethercat"])
def test_dkms_and_ethercat_services_enabled_and_running(host, name):
    service = host.service(name)
    assert service.is_running
    assert service.is_enabled


def test_ethercat_kernel_modules_loaded(host):
    cmd = host.run("/usr/sbin/lsmod")
    assert "ec_master" in cmd.stdout
    assert "ec_generic" in cmd.stdout


@pytest.mark.parametrize("module", ["ec_master", "ec_generic", "mrf", "sis8300drv", "tsc", "pon"])
def test_kernel_modules_present(host, module):
    # modules should automatically be loaded by udev
    # if the required PCI card is present
    # it's not loaded in the VM as there is no PCI card
    # we just check it's present
    cmd = host.run(f"/usr/sbin/modinfo {module}")
    assert f"{module}.ko" in cmd.stdout
