# ics-ans-kernel-modules

Ansible playbook to install kernel modules using DKMS.

This playbook installs a real time kernel and the following modules:

- ethercat (the variable `ethercat_master_device` shall be defined)
- mrfioc2
- sis8300drv
- tsc

WARNING! This playbook targets the `all` group. It should always be run with a limit.

## Local Installation

To run this playbook locally on your CentOS 7 machine, clone this repository and run the following commands inside the directory:


```bash
echo "Install ansible >= 2.8.5"
sudo yum -y install ansible

echo "Create a local Ansible inventory"
cat <<EOF > hosts
localhost ansible_connection=local
EOF

echo "Download the required roles"
ansible-galaxy install -r ./roles/requirements.yml --force -p ./roles

echo "Run the playbook"
sudo ansible-playbook -i hosts playbook.yml
```

## License

BSD 2-clause
